﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        private int XPos;
        private int YPos;
        private static City City;
        
        public CarFactory(int xPos, int yPos, City city)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
        }

        public Car SelectCar(string carType)
        {
            Car car = null;

            switch(carType.ToLower())
            {
                case "1":
                    car = new Sedan(XPos, YPos, City, null);
                    break;
                case "2":
                    car = new Race(XPos, YPos, City, null);
                    break;
                default:
                    car = new Sedan(XPos, YPos, City, null);
                    break;
            }

            return car;
        }
    }
}
