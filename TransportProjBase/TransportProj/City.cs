﻿
using System;
using System.Collections.Generic;
namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
        public static List<Building> buildings = new List<Building>();

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(int xPos, int yPos, string carType)
        {
            CarFactory obj = new CarFactory(xPos, yPos, this);
            return obj.SelectCar(carType);            
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);
            return passenger;
        }

        public void AddBuildingToCity(int xPos, int yPos, Car car)
        {
            Random rand = new Random();

            //check if building is not overlaping with other objects in the City
            while((xPos == car.XPos && yPos == car.YPos) || 
                  (xPos == Passenger.StartingXPos && yPos == Passenger.StartingYPos) ||
                  (xPos == Passenger.DestinationXPos && yPos == Passenger.DestinationYPos) ||
                  (buildings.Find(x => x.XPos == xPos) != null && buildings.Find(x => x.YPos == yPos) != null))
            {
                xPos = rand.Next(XMax - 1);
                yPos = rand.Next(YMax - 1);
            }

            Building building = new Building(xPos, yPos);
            buildings.Add(building);
        }
    }
}