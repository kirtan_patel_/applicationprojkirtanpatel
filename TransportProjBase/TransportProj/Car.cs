﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public int XPreviousPos = -1;
        public int YPreviousPos = -1;
        public string PreviousPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public static City City { get; private set; }
        public bool blocked { get; protected set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            Console.WriteLine("Car starts to pick up passenger.");
            XPos = xPos;
            YPos = yPos;
            XPreviousPos = xPos;
            YPreviousPos = yPos;
            PreviousPos = string.Empty;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }
        
        public bool IsBuildingInWay(int xCarPos, int yCarPos)
        {
            return (City.buildings.Find(x => x.YPos == yCarPos && x.XPos == xCarPos) != null) ? true : false; //check if building in list already
        }

        public async void MoveCarAsync(Passenger passenger)
        {
            if (this.Passenger == null) //has picked up passenger yet?
            {
                if (XPos != Passenger.StartingXPos || YPos != Passenger.StartingYPos) //is car at the pickup location yet?
                {
                    if (XPos < Passenger.StartingXPos && (PreviousPos != "Right" || PreviousPos == null))
                        MoveRight();
                    else if (XPos > Passenger.StartingXPos && (PreviousPos != "Left" || PreviousPos == null))
                        MoveLeft();
                    else if (YPos < Passenger.StartingYPos && (PreviousPos != "Up" || PreviousPos == null))
                        MoveUp();
                    else if (YPos > Passenger.StartingYPos && (PreviousPos != "Down" || PreviousPos == null))
                        MoveDown();
                    else if (XPos == Passenger.StartingXPos && YPos < Passenger.StartingYPos && (PreviousPos != "Up" || PreviousPos == null))
                        MoveUp();
                    else if (XPos == Passenger.StartingXPos && YPos > Passenger.StartingYPos && (PreviousPos != "Down" || PreviousPos == null))
                        MoveDown();
                    else if (YPos == Passenger.StartingYPos && XPos < Passenger.StartingXPos && (PreviousPos != "Right" || PreviousPos == null))
                        MoveRight();
                    else if (YPos == Passenger.StartingYPos && XPos > Passenger.StartingXPos && (PreviousPos != "Left" || PreviousPos == null))
                        MoveLeft();
                    else
                        return;

                    CityPosition.DrawCityMap();
                }
                else 
                    passenger.GetInCar(this); //pick up passenger                 
            }
            else 
            {
                //HttpClient client = new HttpClient();
                //Task<string> loadVeyoSite = client.GetStringAsync("http://www.veyo.com");

                if (XPos < Passenger.DestinationXPos && PreviousPos != "Right")
                    MoveRight();
                else if (XPos > Passenger.DestinationXPos && PreviousPos != "Left")
                    MoveLeft();
                else if (YPos < Passenger.DestinationYPos && PreviousPos != "Up")
                    MoveUp();
                else if (YPos > Passenger.DestinationYPos && PreviousPos != "Down")
                    MoveDown();
                else if (XPos == Passenger.DestinationXPos && YPos < Passenger.DestinationYPos && PreviousPos != "Up")
                    MoveUp();
                else if (XPos == Passenger.DestinationXPos && YPos > Passenger.DestinationYPos && PreviousPos != "Down")
                    MoveDown();
                else if (YPos == Passenger.DestinationYPos && XPos < Passenger.DestinationXPos && PreviousPos != "Right")
                    MoveRight();
                else if (YPos == Passenger.DestinationYPos && XPos > Passenger.DestinationXPos && PreviousPos != "Left")
                    MoveLeft();
                else
                    return;

                CityPosition.DrawCityMap();

                //string response = await loadVeyoSite;
            }            
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
