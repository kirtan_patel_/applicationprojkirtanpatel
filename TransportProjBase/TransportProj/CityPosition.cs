﻿using System;

namespace TransportProj
{
    public class CityPosition
    {
        public static Car Car { get; set; }
        public static Passenger Passenger { get; set; }

        public CityPosition(Car car, Passenger passenger)
        {
            Car = car;
            Passenger = passenger;
        }

        public static void DrawCityMap()
        {
            for (int i = City.YMax - 1; i >= 0; i--)
            {
                for(int j = 0; j <= City.XMax - 1; j++)
                {
                    if (i == Car.YPos && j == Car.XPos)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("C");
                    }
                    else if (i == Passenger.StartingYPos && j == Passenger.StartingXPos)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("S");
                    }
                    else if (i == Passenger.DestinationYPos && j == Passenger.DestinationXPos)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("D");
                    }
                    else if(City.buildings.Find(x => x.YPos == i && x.XPos == j) != null)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write("B");
                    }
                    else
                        Console.Write(".");

                    Console.ResetColor();
                    Console.Write(" ");
                }
                Console.Write("\n");
            }
            Console.Write("\n");
            Console.WriteLine("___________________________________________________________");
            Console.Write("\n");
        }
    }
}
