﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                if (!IsBuildingInWay(XPos, YPos + 1))
                {
                    YPos++;
                    PreviousPos = "Down";
                    WritePositionToConsole();
                }
                else
                {
                    MoveAlternateWay("Up");
                }
            }            
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                if (!IsBuildingInWay(XPos, YPos - 1))
                {
                    YPos--;
                    PreviousPos = "Up";
                    WritePositionToConsole();  
                }
                else
                {
                    MoveAlternateWay("Down");
                }                              
            }           
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                if (!IsBuildingInWay(XPos + 1, YPos))
                {
                    XPos++;
                    PreviousPos = "Left";
                    WritePositionToConsole();
                }
                else
                {
                    MoveAlternateWay("Right");
                }                                
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                if (!IsBuildingInWay(XPos - 1, YPos))
                {
                    XPos--;
                    PreviousPos = "Right";
                    WritePositionToConsole();
                }
                else
                {
                    MoveAlternateWay("Left");
                }                
            }
        }

        private void MoveAlternateWay(string CurrentMove)
        {
            if(CurrentMove == "Up")
            {
                switch(PreviousPos)
                {
                    case "Down":
                        if (!IsBuildingInWay(XPos-1, YPos) && XPos > 0)
                            MoveLeft();
                        if (!IsBuildingInWay(XPos+1, YPos) && XPos < City.XMax)
                            MoveRight();
                        else
                            MoveDown();
                        break;
                    case "Left":
                        if (!IsBuildingInWay(XPos + 1, YPos) && XPos < City.XMax)
                            MoveRight();
                        if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();                        
                        else
                            MoveLeft();
                        break;
                    case "Right":
                        if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        if (!IsBuildingInWay(XPos-1, YPos) && XPos < City.XMax)
                            MoveLeft();
                        else
                            MoveRight();
                        break;                    
                }
            }
            else if (CurrentMove == "Down")
            {
                switch (PreviousPos)
                {
                    case "Up":
                        if (!IsBuildingInWay(XPos-1, YPos) && XPos < City.XMax)
                            MoveLeft();
                        if (!IsBuildingInWay(XPos+1, YPos) && XPos > 0)
                            MoveRight();
                        else
                            MoveUp();
                        break;
                    case "Left":                        
                        if (!IsBuildingInWay(XPos + 1, YPos) && XPos > 0)
                            MoveRight();
                        else if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        else
                            MoveLeft();
                        break;
                    case "Right":
                        if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        if (!IsBuildingInWay(XPos-1, YPos) && XPos < City.XMax)
                            MoveLeft();
                        else
                            MoveRight();
                        break;
                }
            }
            else if (CurrentMove == "Right")
            {
                switch (PreviousPos)
                {
                    case "Left":
                        if (!IsBuildingInWay(XPos, YPos+1) && YPos < City.YMax)
                            MoveUp();
                        else if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        else
                            MoveLeft();
                        break;
                    case "Up":
                        if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        else if (!IsBuildingInWay(XPos-1, YPos) && XPos < City.XMax)
                            MoveLeft();
                        else
                            MoveUp();
                        break;
                    case "Down":
                        if (!IsBuildingInWay(XPos-1, YPos) && XPos < City.XMax)
                            MoveLeft();
                        else if (!IsBuildingInWay(XPos, YPos+1) && YPos < City.YMax)
                            MoveUp();
                        else
                            MoveDown();
                        break;
                    default:
                            MoveDown();
                        break;
                }
            }
            if (CurrentMove == "Left")
            {
                switch (PreviousPos)
                {
                    case "Right":
                        if (!IsBuildingInWay(XPos, YPos+1) && YPos < City.YMax)
                            MoveUp();
                        else if (!IsBuildingInWay(XPos, YPos-1) && YPos > 0)
                            MoveDown();
                        else
                            MoveRight();
                        break;
                    case "Up":
                        if (!IsBuildingInWay(XPos, YPos - 1) && YPos > 0)
                            MoveDown();
                        else if (!IsBuildingInWay(XPos+1, YPos) && XPos > 0)
                            MoveRight();                        
                        else
                            MoveUp();
                        break;
                    case "Down":
                        if (!IsBuildingInWay(XPos + 1, YPos) && XPos > 0)
                            MoveRight();
                        else if (!IsBuildingInWay(XPos, YPos+1) && YPos < City.YMax)
                            MoveUp();                        
                        else
                            MoveDown();
                        break;
                    default:
                            MoveDown();
                        break;
                }
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }        
    }
}
