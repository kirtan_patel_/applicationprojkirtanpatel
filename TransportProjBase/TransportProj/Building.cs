﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class Building
    {
        public int XPos { get; private set; }
        public int YPos { get; private set; }

        public Building(int xPos, int yPos)
        {            
            this.XPos = xPos;
            this.YPos = yPos;
        }
    }
}
