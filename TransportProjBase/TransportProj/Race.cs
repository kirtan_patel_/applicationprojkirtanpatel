﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportProj
{
    class Race : Car
    {
        public Race(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        { }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                if (IsBuildingInWay(XPos, YPos + 2))
                {
                    MoveLeft();
                }
                else
                {
                    if (this.Passenger == null)
                    {
                        if (YPos < Passenger.StartingYPos - 1)
                            YPos = YPos + 2;
                        else
                            YPos++;
                    }
                    else
                        if (YPos < Passenger.DestinationYPos - 1)
                            YPos = YPos + 2;
                        else
                            YPos++;
                }

                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                if (IsBuildingInWay(XPos + 2, YPos))
                {
                    MoveRight();
                }
                else
                {
                    if (this.Passenger == null)
                    {
                        if (YPos > Passenger.StartingYPos + 1)
                            YPos = YPos - 2;
                        else
                            YPos--;
                    }
                    else
                        if (YPos > Passenger.DestinationYPos + 1)
                            YPos = YPos - 2;
                        else
                            YPos--;
                }

                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                if (IsBuildingInWay(XPos + 2, YPos))
                {
                    MoveUp();
                }
                else
                {
                    if (this.Passenger == null)
                    {
                        if (XPos < Passenger.StartingXPos - 1)
                            XPos = XPos + 2;
                        else
                            XPos++;
                    }
                    else
                    {
                        if (XPos < Passenger.DestinationXPos - 1)
                            XPos = XPos + 2;
                        else
                            XPos++;
                    }
                }

                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                if (IsBuildingInWay(XPos - 2, YPos))
                {
                    MoveDown();
                }
                else
                {
                    if (this.Passenger == null)
                    {
                        if (XPos > Passenger.StartingXPos + 1)
                            XPos = XPos - 2;
                        else
                            XPos--;
                    }
                    else
                    {
                        if (XPos > Passenger.DestinationXPos + 1)
                            XPos = XPos - 2;
                        else
                            XPos--;
                    }
                }

                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Race Car moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
