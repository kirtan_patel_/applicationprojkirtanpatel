﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            Console.WriteLine("Select Car Type:");
            Console.WriteLine("1. Sedan");
            Console.WriteLine("2. Race");
            string cartype = Console.ReadLine();

            try
            {
                City MyCity = new City(CityLength, CityWidth);
                Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), cartype);
                Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));               

                for (int i = 0; i < 5; i++)
                {
                    MyCity.AddBuildingToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), car);
                }

                CityPosition cityPos = new CityPosition(car, passenger);
                CityPosition.DrawCityMap();

                while (!passenger.IsAtDestination())
                {
                    Tick(car, passenger);
                }

                passenger.GetOutOfCar(); //Reached destination

                Console.ReadKey();
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception Occured");
            }
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            car.MoveCarAsync(passenger);                      
        }
    }
}
